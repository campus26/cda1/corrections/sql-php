<?php

use CDA\Bootstrap;

require __DIR__ . '/../vendor/autoload.php';

Bootstrap::run();
