<?php

namespace CDA;

use CDA\Routers\Routers;
use Slim\Factory\AppFactory;
use CDA\Utils\ServiceContainer;
use CDA\Utils\Services;
use Slim\Middleware\MethodOverrideMiddleware;
use Symfony\Component\Dotenv\Dotenv;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

class Bootstrap
{

    /**
     * Run the application.
     */
    public static function run()
    {
        // Load environment variables from .env file
        $dotenv = new Dotenv();
        $dotenv->loadEnv(__DIR__ . '/../.env');

        // Create service container
        $container = new ServiceContainer();

        // Setup services
        $services = new Services($container);
        $services->setupServices();

        // Set service container for application factory
        AppFactory::setContainer($container->getContainer());

        // Create the application
        $app = AppFactory::create();

        // Create Twig instance for rendering views
        // Disable Twig cache for development
        $twig = Twig::create(__DIR__ . '/Views', ['cache' => false]);
        $app->add(TwigMiddleware::create($app, $twig));

        // Add routing middleware
        $app->addRoutingMiddleware();

        // Add error handling middleware
        // Enable error handling and display errors in development
        $app->addErrorMiddleware(true, true, true, $container->getContainer()->get('logger'));

        // Add method override middleware
        $app->add(new MethodOverrideMiddleware);

        // Setup routes
        Routers::setupRoutes($app);

        // Run the application
        $app->run();
    }
}
