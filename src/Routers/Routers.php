<?php

namespace CDA\Routers;

use Slim\App;

class Routers
{
    /**
     * Sets up the routes for the application.
     *
     * @param App $app The application instance.
     */
    public static function setupRoutes(App $app)
    {
        // Set up the routes for the home controller
        HomeRouter::setupRoutes($app);

        // Set up the routes for the server controller
        ServerRouter::setupRoutes($app);
    }
}
