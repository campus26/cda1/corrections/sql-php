<?php

namespace CDA\Routers\Interfaces;

use Slim\App;

interface RouterInterface
{
    static public function setupRoutes(App $app);
}
