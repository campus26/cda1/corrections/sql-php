<?php

namespace CDA\Routers;

use CDA\Routers\Interfaces\RouterInterface;
use Slim\App;

class HomeRouter implements RouterInterface
{
    // Define a static public function called setupRoutes that takes an instance of the App class as a parameter
    static public function setupRoutes(App $app)
    {
        // Create a route group at the root path
        $app->group('/', function ($group) {
            // Define a GET route with an empty path that maps to the HomeController's index method
            $group->get('', 'CDA\Controllers\HomeController:index');
        });
    }
}
