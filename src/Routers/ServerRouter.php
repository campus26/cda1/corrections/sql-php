<?php

namespace CDA\Routers;

use CDA\Routers\Interfaces\RouterInterface;
use Slim\App;

class ServerRouter implements RouterInterface
{
    static public function setupRoutes(App $app)
    {
        $app->group('/servers', function ($group) {
            $group->get('/create', 'CDA\Controllers\ServerController:createView');
            $group->get('/update/{id}', 'CDA\Controllers\ServerController:updateView');
            $group->get('/{id}', 'CDA\Controllers\ServerController:getOne');
            $group->post('/', 'CDA\Controllers\ServerController:create');
            $group->put('/{id}', 'CDA\Controllers\ServerController:update');
            $group->delete('/{id}', 'CDA\Controllers\ServerController:delete');
        });
    }
}
