<?php

namespace CDA\Models;

use DI\Container;
use CDA\Models\Server\ServerEntity;
use CDA\Models\Server\ServerRepository;

class Models
{
    private $container;
    private $models = [];

    /**
     * Create a new instance of the class.
     *
     * @param Container $container The dependency injection container.
     */
    public function __construct(Container $container)
    {
        // Set the dependency injection container
        $this->container = $container;

        // Define an array of models with their respective entity and repository classes
        $this->models = [
            'server' => [
                'entity' => ServerEntity::class,
                'repository' => ServerRepository::class
            ]
        ];
    }

    /**
     * Retrieves the entity associated with the given name.
     *
     * @param string $name The name of the entity.
     * @return mixed The entity associated with the given name.
     */
    public function getEntity(string $name)
    {
        // Access the 'models' array using the given name as the key,
        // and retrieve the 'entity' value associated with it.
        return $this->models[$name]['entity'];
    }

    /**
     * Retrieves a repository based on the given name.
     *
     * @param string $name The name of the repository.
     * @return object The instantiated repository.
     */
    public function getRepository(string $name)
    {
        // Retrieve the repository class name from the models array using the given name as the key
        $repositoryClassName = $this->models[$name]['repository'];

        // Create a new instance of the repository class, passing in the container as an argument
        $repositoryInstance = new $repositoryClassName($this->container);

        // Return the instantiated repository
        return $repositoryInstance;
    }
}
