<?php

namespace CDA\Models\Server;

use CDA\Models\Interfaces\EntityInterface;
use DI\Container;

class ServerEntity implements EntityInterface
{
    private $db;
    private $id;
    private $host_name;
    private $ip_address;
    private $name;

    public function __construct(Container $container)
    {
        $this->db = $container->get('db');
    }

    /**
     * Hydrates the object with data from an array.
     *
     * @param array $data The array containing the data to hydrate the object with.
     * @return void
     */
    public function hydrate(array $data)
    {
        // Check if the 'id' key exists in the array
        if (isset($data['id'])) {
            // Assign the value of the 'id' key to the object's 'id' property
            $this->id = $data['id'];
        }

        // Check if the 'host_name' key exists in the array
        if (isset($data['host_name'])) {
            // Assign the value of the 'host_name' key to the object's 'host_name' property
            $this->host_name = $data['host_name'];
        }

        // Check if the 'ip_address' key exists in the array
        if (isset($data['ip_address'])) {
            // Assign the value of the 'ip_address' key to the object's 'ip_address' property
            $this->ip_address = $data['ip_address'];
        }

        // Check if the 'name' key exists in the array
        if (isset($data['name'])) {
            // Assign the value of the 'name' key to the object's 'name' property
            $this->name = $data['name'];
        }
    }

    /**
     * Get the value of name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of ip_address
     */
    public function getIp_address()
    {
        return $this->ip_address;
    }

    /**
     * Set the value of ip_address
     *
     * @return  self
     */
    public function setIp_address($ip_address)
    {
        $this->ip_address = $ip_address;

        return $this;
    }

    /**
     * Get the value of host_name
     */
    public function getHost_name()
    {
        return $this->host_name;
    }

    /**
     * Set the value of host_name
     *
     * @return  self
     */
    public function setHost_name($host_name)
    {
        $this->host_name = $host_name;

        return $this;
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Insert a new server record into the database.
     */
    public function insert()
    {
        // Define the SQL query to insert a new server record.
        $sql = "INSERT INTO servers (host_name, ip_address, name) VALUES (:host_name, :ip_address, :name)";

        // Prepare the query statement.
        $query = $this->db->prepare($sql);

        // Execute the query with the specified parameter values.
        $query->execute([
            'host_name' => $this->host_name,
            'ip_address' => $this->ip_address,
            'name' => $this->name
        ]);
    }

    public function update()
    {
        // Prepare the SQL statement to update the server record in the database
        $sql = "UPDATE servers SET host_name = :host_name, ip_address = :ip_address, name = :name WHERE id = :id";

        // Prepare the query using the prepared statement
        $query = $this->db->prepare($sql);

        // Execute the query with the updated values
        $query->execute([
            'host_name' => $this->host_name,
            'ip_address' => $this->ip_address,
            'name' => $this->name,
            'id' => $this->id
        ]);
    }

    /**
     * Delete the server record from the database.
     */
    public function delete()
    {
        // Define the SQL query to delete the server with a specific ID.
        $sql = "DELETE FROM servers WHERE id = :id";

        // Prepare the query statement.
        $query = $this->db->prepare($sql);

        // Execute the query with the server ID as a parameter.
        $query->execute([
            'id' => $this->id
        ]);
    }
}
