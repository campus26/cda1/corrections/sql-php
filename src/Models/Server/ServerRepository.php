<?php

namespace CDA\Models\Server;

use CDA\Models\Interfaces\RepositoryInterface;
use DI\Container;
use PDO;

class ServerRepository implements RepositoryInterface
{
    private $db;
    private $entity;
    private $container;

    public function __construct(Container $container)
    {
        // Assign the 'db' service from the container to the $this->db property
        $this->db = $container->get('db');

        // Retrieve the 'models' service from the container and get the 'server' entity
        $modelsService = $container->get('models');
        $this->entity = $modelsService->getEntity('server');

        // Assign the container object to the $this->container property
        $this->container = $container;
    }

    public function findAll()
    {
        // Prepare the SQL query to select all records from the "servers" table
        $query = $this->db->prepare('SELECT * FROM servers');

        // Execute the query
        $query->execute();

        // Fetch all the results from the query
        $data = $query->fetchAll();

        // Create an empty array to store the server entities
        $servers = [];

        // Iterate over each row in the fetched data
        foreach ($data as $row) {
            // Create a new instance of the entity class
            $entity = new $this->entity($this->container);

            // Hydrate the entity with the data from the row
            $entity->hydrate($row);

            // Add the hydrated entity to the servers array
            $servers[] = $entity;
        }

        // Return the array of server entities
        return $servers;
    }

    /**
     * Find and return a single entity by ID.
     *
     * @param string $id The ID of the entity to find.
     * @return Entity|null The found entity, or null if not found.
     */
    public function findOne(string $id)
    {
        // Prepare the SQL query to select a row from the 'servers' table where the ID matches
        $query = $this->db->prepare('SELECT * FROM servers where id = :id');

        // Execute the query with the ID as a parameter
        $query->execute([':id' => $id]);

        // Fetch the data from the executed query
        $data = $query->fetch(PDO::FETCH_ASSOC);

        // If no data is found, return null
        if (!$data) {
            return null;
        }

        // Create a new instance of the entity class, passing the container as a parameter
        $entity = new $this->entity($this->container);

        // Hydrate the entity with the fetched data
        $entity->hydrate($data);

        // Return the populated entity
        return $entity;
    }
}
