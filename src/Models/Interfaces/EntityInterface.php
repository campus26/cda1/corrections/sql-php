<?php

namespace CDA\Models\Interfaces;

use DI\Container;

interface EntityInterface
{
    public function __construct(Container $container);
    public function hydrate(array $data);

    public function insert();
    public function update();
    public function delete();
}
