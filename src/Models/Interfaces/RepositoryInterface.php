<?php

namespace CDA\Models\Interfaces;

use DI\Container;

interface RepositoryInterface
{
    public function __construct(Container $container);

    public function findOne(string $id);
    public function findAll();
}
