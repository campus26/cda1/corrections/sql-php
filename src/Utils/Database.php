<?php

namespace CDA\Utils;

use PDO;
use PDOException;

class Database
{
    private $host;
    private $dbname;
    private $username;
    private $password;
    private $conn;

    /**
     * Constructs a new instance of the database.
     *
     * @throws PDOException If the connection fails.
     */
    public function __construct()
    {
        // Get the database host from the environment variables or the $_ENV array
        $this->host = $_ENV['DB_HOST'] ?? getenv('DB_HOST');

        // Get the database name from the environment variables or the $_ENV array
        $this->dbname = $_ENV['DB_NAME'] ?? getenv('DB_NAME');

        // Get the database username from the environment variables or the $_ENV array
        $this->username = $_ENV['DB_USER'] ?? getenv('DB_USER');

        // Get the database password from the environment variables or the $_ENV array
        $this->password = $_ENV['DB_PASSWORD'] ?? getenv('DB_PASSWORD');

        // Create the Data Source Name (DSN) string for the PDO connection
        $dsn = "mysql:host={$this->host};dbname={$this->dbname}";

        try {
            // Create a new PDO connection with the provided DSN, username, and password
            $this->conn = new PDO($dsn, $this->username, $this->password);

            // Set the error mode to throw exceptions for better error handling
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            // If an exception is caught, display the error message
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function getConnection()
    {
        return $this->conn;
    }
}
