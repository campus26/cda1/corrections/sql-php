<?php

namespace CDA\Utils;

use CDA\Models\Models;
use CDA\Utils\ServiceContainer;
use CDA\Utils\Database;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


class Services
{
    private ServiceContainer $ServiceContainer;

    /**
     * Constructor for the class.
     *
     * @param ServiceContainer $serviceContainer The service container instance.
     */
    public function __construct(ServiceContainer $serviceContainer)
    {
        $this->ServiceContainer = $serviceContainer;
    }

    public function setupServices()
    {
        // Add a 'logger' service to the ServiceContainer
        $this->ServiceContainer->addService('logger', function () {
            // Create a new instance of the Logger class with the name 'app'
            $logger = new Logger('app');

            // Create a new StreamHandler that writes logs to the specified file path
            $streamHandler = new StreamHandler(__DIR__ . '/../../var/logs/log', 100);

            // Push the StreamHandler to the logger's handler stack
            $logger->pushHandler($streamHandler);

            // Return the logger instance
            return $logger;
        });

        // Add a 'db' service to the ServiceContainer
        $this->ServiceContainer->addService('db', function () {
            // Create a new instance of the Database class
            $db = new Database();

            // Get the database connection from the Database instance and return it
            return $db->getConnection();
        });

        // Add a 'models' service to the ServiceContainer
        $this->ServiceContainer->addService('models', function () {
            // Create a new instance of the Models class with the ServiceContainer's container
            return new Models($this->ServiceContainer->getContainer());
        });
    }
}
