<?php

namespace CDA\Utils;

use DI\Container;

class ServiceContainer
{
    private $Container;

    public function __construct()
    {
        $this->Container = new Container();
    }

    /**
     * Add a service to the container.
     *
     * @param string $name The name of the service.
     * @param callable $callable The callable that defines the service.
     * @return void
     */
    public function addService(string $name, callable $callable)
    {
        // Set the service in the container with the given name and callable.
        $this->Container->set($name, $callable);
    }

    /**
     * Returns the container object.
     *
     * @return Container The container object.
     */
    public function getContainer()
    {
        // Return the container object.
        return $this->Container;
    }
}
