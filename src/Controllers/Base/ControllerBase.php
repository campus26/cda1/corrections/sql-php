<?php

namespace CDA\Controllers\Base;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

abstract class ControllerBase
{
    protected $container;

    /**
     * Constructor for the class.
     *
     * @param ContainerInterface $container The dependency injection container.
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Redirects the response to a specified location with an optional status code.
     *
     * @param \Psr\Http\Message\ResponseInterface $response The response object.
     * @param string $location The URL to redirect to.
     * @param int $statusCode The HTTP status code to use for the redirect (defaults to 302).
     *
     * @return \Psr\Http\Message\ResponseInterface The updated response object.
     */
    protected function redirect(ResponseInterface $response, string $location, int $statusCode = 302): ResponseInterface
    {
        // Set the 'Location' header of the response to the specified location.
        $response = $response->withHeader('Location', $location);

        // Set the status code of the response to the specified status code.
        $response = $response->withStatus($statusCode);

        // Return the updated response object.
        return $response;
    }
}
