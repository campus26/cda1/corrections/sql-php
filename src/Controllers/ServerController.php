<?php

namespace CDA\Controllers;

use CDA\Controllers\Base\ControllerBase;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Slim\Views\Twig;

class ServerController extends ControllerBase
{

    /**
     * Retrieves a single server.
     *
     * @param Request $request The HTTP request object.
     * @param Response $response The HTTP response object.
     * @param array $args The route parameters.
     * @return Response The HTTP response.
     */
    public function getOne(Request $request, Response $response, array $args): Response
    {
        // Get the server repository from the models container
        $serverRepository = $this->container->get("models")->getRepository('server');

        // Find the server by its ID
        $server = $serverRepository->findOne($args["id"]);

        // If the server is not found, redirect to the home page with an error message
        if (empty($server)) {
            return $this->redirect($response, "/?success=false&message=Le serveur demandé n'existe pas");
        }

        // Create a Twig view from the request
        $view = Twig::fromRequest($request);

        // Render the server single template with the server data
        return $view->render($response, 'server/single.html.twig', ['server' => $server]);
    }

    /**
     * Output the create form for creating a new server.
     *
     * @param Request $request The request object.
     * @param Response $response The response object.
     * @param mixed $args Additional arguments.
     * @return Response The HTTP response.
     */
    public function createView(Request $request, Response $response, $args): Response
    {
        // Create a Twig view using the request object
        $view = Twig::fromRequest($request);

        // Initialize the error variable as false
        $error = false;

        // Check if the 'success' query parameter is set in the request
        if (isset($request->getQueryParams()["success"])) {
            // If the 'success' query parameter is set to "false", set the error variable to true
            $error = $request->getQueryParams()["success"] === "false";
        }

        // Render the 'createView.html.twig' template, passing the error value and the previous query parameters
        return $view->render($response, 'server/createView.html.twig', ["error" => $error, 'previous' => $request->getQueryParams()]);
    }

    /**
     * Create a server
     *
     * @param Request $request The request object
     * @param Response $response The response object
     * @param array $args The route arguments
     * @return Response The response object
     * @throws \PDOException
     */
    public function create(Request $request, Response $response, array $args): Response
    {
        try {
            // Get the parsed body from the request
            $infos = $request->getParsedBody();

            // Get the server entity from the container
            $serverEntity = $this->container->get('models')->getEntity('server');

            // Instantiate a new server object
            $server = new $serverEntity($this->container);

            // Populate the server object with the provided information
            $server->hydrate($infos);

            // Insert the server into the database
            $server->insert();

            // Redirect the user to the home page with a success message
            return $this->redirect($response, "/?success=true&message=Le serveur a bien été crée");
        } catch (\PDOException) {
            // If there is a PDO exception, redirect the user back to the create server page with an error message
            return $this->redirect($response, "/servers/create?success=false&name={$infos["name"]}&ip_address={$infos["ip_address"]}&host_name={$infos["host_name"]}");
        }
    }

    /**
     * Output the update form for modifying the new server.
     *
     * @param Request $request The HTTP request object.
     * @param Response $response The HTTP response object.
     * @param array $args An array of route parameters.
     * @return Response The HTTP response object.
     */
    public function updateView(Request $request, Response $response, array $args): Response
    {
        // Create a Twig view object from the request.
        $view = Twig::fromRequest($request);

        // Initialize the error flag as false.
        $error = false;

        // Check if the 'success' query parameter exists in the request and set the error flag accordingly.
        if ($request->getQueryParams()["success"] ?? false) {
            $error = $request->getQueryParams()["success"] === "false";
        }

        // Get the server repository from the container and find the server with the given ID.
        $serverRepository = $this->container->get('models')->getRepository('server');
        $server = $serverRepository->findOne($args["id"]);

        // If the server is empty, redirect the user to the homepage with an error message.
        if (empty($server)) {
            return $this->redirect($response, "/?success=false&message=Le serveur demandé n'existe pas");
        }

        // Render the update view template and pass the error flag and server object to it.
        return $view->render($response, 'server/updateView.html.twig', ["error" => $error, 'server' => $server]);
    }

    /**
     * Update Server information
     *
     * @param Request $request The HTTP request object.
     * @param Response $response The HTTP response object.
     * @param array $args An array of route parameters.
     * @return Response The HTTP response object.
     */
    public function update(Request $request, Response $response, array $args)
    {
        try {
            // Get the request body as an array
            $infos = $request->getParsedBody();

            // Get the server repository from the container
            $serverRepository = $this->container->get('models')->getRepository('server');

            // Find the server by its ID
            $server = $serverRepository->findOne($args["id"]);

            // If server does not exist, redirect with error message
            if (empty($server)) {
                return $this->redirect($response, "/?success=false&message=Le serveur demandé n'existe pas");
            }

            // Update the server object with the new information
            $server->hydrate($infos);

            // Save the updated server object
            $server->update();

            // Redirect with success message
            return $this->redirect($response, "/?success=true&message=Le serveur a bien été modifié");
        } catch (\PDOException) {
            // If there is a database error, redirect with error message
            return $this->redirect($response, "/servers/update/{$args["id"]}?success=false");
        }
    }

    /**
     * Delete Server information
     *
     * This function deletes server information based on the provided server ID.
     *
     * @param Request $request The HTTP request object.
     * @param Response $response The HTTP response object.
     * @param array $args An array of route parameters.
     * @return Response The HTTP response object.
     */
    public function delete(Request $request, Response $response, array $args)
    {
        try {
            // Get the server repository from the container
            $serverRepository = $this->container->get("models")->getRepository('server');

            // Find the server based on the provided server ID
            $server = $serverRepository->findOne($args["id"]);

            // Check if the server exists
            if (empty($server)) {
                // Redirect with an error message if the server does not exist
                return $this->redirect($response, "/?success=false&message=Le serveur demandé n'existe pas");
            }

            // Delete the server
            $server->delete();

            // Redirect with a success message
            return $this->redirect($response, "/?success=true&message=Le serveur a bien été supprimé");
        } catch (\PDOException) {
            // Redirect with an error message if there is a database error
            return $this->redirect($response, "/?success=false&message=Le serveur demandé n'existe pas");
        }
    }
}
