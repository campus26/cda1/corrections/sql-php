<?php

namespace CDA\Controllers;

use CDA\Controllers\Base\ControllerBase;
use Slim\Psr7\Request;
use Slim\Psr7\Response;
use Slim\Views\Twig;

class HomeController extends ControllerBase
{

    /**
     * Retrieves the server data and renders the 'home/index.html.twig' template.
     *
     * @param Request $request The HTTP request object.
     * @param Response $response The HTTP response object.
     * @param mixed $args Additional arguments.
     * @return mixed The rendered view.
     */
    public function index(Request $request, Response $response, $args)
    {
        // Get the server repository from the container
        $serverRepository = $this->container->get("models")->getRepository('server');

        // Check if the 'success' query parameter is set in the request
        $success = null;
        if (isset($request->getQueryParams()["success"])) {
            // If 'success' is set, assign the value of the 'message' query parameter to the $success variable
            $success = $request->getQueryParams()["message"];
        }

        // Create a Twig view based on the request
        $view = Twig::fromRequest($request);

        // Render the 'home/index.html.twig' template with the server repository data and the 'success' variable
        return $view->render($response, 'home/index.html.twig', [
            'servers' => $serverRepository->findAll(),
            "success" => $success
        ]);
    }
}
