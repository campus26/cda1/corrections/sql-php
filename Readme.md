# Correction brief PHP / BDD

## Initialiser le projet

**Pensez à créer votre fichier .env afin de pouvoir connecter votre base de données**
Le fichier .env sera sous cette forme là :

```
DB_HOST=adresse de votre bdd sous cette forme là = hote:port
DB_NAME=nom de votre bdd
DB_USER= votre utilisateur
DB_PASSWORD= votre mot de passe
```

Une fois que vous avez réalisé cela, il nous manque plus qu'à démarrer le serveur de cette manière :

```bash
cd public
php -S localhost:8888
```

Vous êtes bien sûr libre d'utiliser le port que vous souhaitez

il ne vous restera plus qu'à vous rendre sur votre [http://localhost:8888](localhost) pour voir l'application en fonctionnement.
